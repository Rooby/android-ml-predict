package com.example.mlpredict;

import android.app.Application;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class MMApplication extends Application{
    public static RequestQueue queue;
    public static MMApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    public static RequestQueue getQue() {
        if (queue == null) {
            queue = Volley.newRequestQueue(sInstance);
        }
        return queue;
    }
}
