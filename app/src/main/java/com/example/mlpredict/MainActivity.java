package com.example.mlpredict;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private TextView activityNameTextView;
    private TextView numberOfRequestsTextView;

    private SensorManager mSensorManager;

    private Sensor mAccelerometer;
    private Sensor mGyroscope;

    private long graphAccLastXValue = 0;
    private long graphGyroLastXValue = 0;

    private LineGraphSeries<DataPoint> xAccSeries = new LineGraphSeries<DataPoint>(new DataPoint[]{
            new DataPoint(0, 0)
    });
    private LineGraphSeries<DataPoint> yAccSeries = new LineGraphSeries<DataPoint>(new DataPoint[]{
            new DataPoint(0, 0)
    });;
    private LineGraphSeries<DataPoint> zAccSeries = new LineGraphSeries<DataPoint>(new DataPoint[]{
            new DataPoint(0, 0)
    });;

    private LineGraphSeries<DataPoint> xGyroSeries = new LineGraphSeries<DataPoint>(new DataPoint[]{
            new DataPoint(0, 0)
    });
    private LineGraphSeries<DataPoint> yGyroSeries = new LineGraphSeries<DataPoint>(new DataPoint[]{
            new DataPoint(0, 0)
    });;
    private LineGraphSeries<DataPoint> zGyroSeries = new LineGraphSeries<DataPoint>(new DataPoint[]{
            new DataPoint(0, 0)
    });;

    private int requestsCounter = 0;

    private ArrayList<Float> accData = new ArrayList<>();
    private ArrayList<Float> gyroData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initAccelerometer();
        initGyroscope();

        activityNameTextView = findViewById(R.id.activity_name);
        numberOfRequestsTextView = findViewById(R.id.number_requests);

        initAccelerometer();
        initGyroscope();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mGyroscope, SensorManager.SENSOR_DELAY_FASTEST);
    }

    private void predict(JSONObject data) {
        String url = "http://192.168.0.107";
        final String mRequestBody = data.toString();

        final SensorEventListener context = this;

        accData.clear();
        gyroData.clear();

        mSensorManager.registerListener(context, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(context, mGyroscope, SensorManager.SENSOR_DELAY_FASTEST);

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST, url + "/api/model/predict",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Volley", "Response is: " + response);
                        Gson gson = new Gson();
                        Type predictType = new TypeToken<PredictResponse>() {}.getType();
                        PredictResponse predictedActivity = gson.fromJson(response, predictType);
                        activityNameTextView.setText(predictedActivity.getMessage());

                        requestsCounter++;
                        numberOfRequestsTextView.setText(Integer.toString(requestsCounter));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Volley", error.getMessage());
                    }
                }
            ) {
                @Override
                public Map<String, String> getHeaders() {
                    final Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }
                @Override
                public byte[] getBody() {
                    try {
                        return mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = MMApplication.getQue();
        queue.add(stringRequest);
    }

    private void initAccelerometer() {
        xAccSeries.setColor(Color.RED);
        yAccSeries.setColor(Color.GREEN);
        zAccSeries.setColor(Color.BLUE);

        GraphView accGraph = (GraphView) findViewById(R.id.acc_graph);

        accGraph.addSeries(xAccSeries);
        accGraph.addSeries(yAccSeries);
        accGraph.addSeries(zAccSeries);

        Viewport accViewport = accGraph.getViewport();

        accViewport.setXAxisBoundsManual(true);
        accViewport.setMaxX(10);
        accViewport.setMinX(-10);

        accGraph.getViewport().setYAxisBoundsManual(true);
        accViewport.setMaxY(10);
        accViewport.setMinY(-10);

        accViewport.setScrollable(true);
        accGraph.getViewport().setScalableY(true);
        accGraph.getViewport().setScrollableY(true);
    }

    private void initGyroscope() {
        xGyroSeries.setColor(Color.RED);
        yGyroSeries.setColor(Color.GREEN);
        zGyroSeries.setColor(Color.BLUE);

        GraphView gyroGraph = (GraphView) findViewById(R.id.gyro_graph);

        gyroGraph.addSeries(xGyroSeries);
        gyroGraph.addSeries(yGyroSeries);
        gyroGraph.addSeries(zGyroSeries);

        Viewport gyroViewport = gyroGraph.getViewport();

        gyroViewport.setXAxisBoundsManual(true);
        gyroViewport.setMaxX(10);
        gyroViewport.setMinX(-10);

        gyroGraph.getViewport().setYAxisBoundsManual(true);
        gyroViewport.setMaxY(10);
        gyroViewport.setMinY(-10);

        gyroViewport.setScrollable(true);
        gyroGraph.getViewport().setScalableY(true);
        gyroGraph.getViewport().setScrollableY(true);
    }

    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mGyroscope, SensorManager.SENSOR_DELAY_FASTEST);
    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void stopScan() {
        if (graphAccLastXValue % 128 == 0) {
            mSensorManager.unregisterListener(this, mAccelerometer);
        }
        if (graphGyroLastXValue % 128 == 0) {
            mSensorManager.unregisterListener(this, mGyroscope);
        }
        if (graphAccLastXValue % 128 != 0 || graphGyroLastXValue % 128 != 0) {
            return;
        }

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("accData", accData);
            jsonBody.put("gyroData", gyroData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        predict(jsonBody);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        switch(event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                displayAccValuesInChart(event);
                break;

            case Sensor.TYPE_GYROSCOPE:
                displayGyroValuesInChart(event);
                break;
        }
    }

    private void displayAccValuesInChart(SensorEvent event) {
        graphAccLastXValue++;

        accData.add(event.values[0]);
        accData.add(event.values[1]);
        accData.add(event.values[2]);

        xAccSeries.appendData(new DataPoint(graphAccLastXValue, event.values[0]), true, 10000);
        yAccSeries.appendData(new DataPoint(graphAccLastXValue, event.values[1]), true, 10000);
        zAccSeries.appendData(new DataPoint(graphAccLastXValue, event.values[2]), true, 10000);

        if (graphAccLastXValue % 128 == 0) {
            stopScan();
        }

    }

    private void displayGyroValuesInChart(SensorEvent event) {
        graphGyroLastXValue++;

        gyroData.add(event.values[0]);
        gyroData.add(event.values[1]);
        gyroData.add(event.values[2]);

        xGyroSeries.appendData(new DataPoint(graphGyroLastXValue, event.values[0]), true, 10000);
        yGyroSeries.appendData(new DataPoint(graphGyroLastXValue, event.values[1]), true, 10000);
        zGyroSeries.appendData(new DataPoint(graphGyroLastXValue, event.values[2]), true, 10000);

        if (graphGyroLastXValue % 128 == 0) {
            stopScan();
        }
    }
}
